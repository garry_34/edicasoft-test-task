export const state = () => ({
  users: [],
  edit_dialog_value: false
})

export const mutations = {
  setUsers (state, payload) {
    state.users = payload
  },
  setDialogValue (state, payload) {
    state.edit_dialog_value = payload
  },
}
export const actions = {
  getUsers ({ commit }) {
    //Sending a request to get a list of users
    this.$axios.get(`https://jsonplaceholder.typicode.com/users`)
      .then(res => {
        commit('setUsers', res.data)//
      })
      .catch(err => {
        console.log('error', err);
      });
  }
}
